## Preresequites

1. NPM & Node.js - https://nodejs.org/en/
2. `npm install node-sass@beta`

## Run the starting point

1. `npm i`
2. `npm audit fix`
2. `npm run seed`
3. `npm run build`
4. `npm start`
5. Navigate to http://127.0.0.1:3000/ on local machine

## Directory structure
`/assets` contains stylesheets.

`/client` contains React components and a Redux store.

`/files` is an empty directory for storing patient files.

`/public` is where the code from `/client` is compiled to.

`/server` contains an Express server, routes, and the database config.

## Logging in
Running the seed script seeds the database, including some users we've created for you. You can use any user's email address and password to log in to the app as that user.
